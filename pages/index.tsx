import Header from '../components/nav/header/Header';
import { NextPageWithLayout } from './page';

const Home: NextPageWithLayout = () => {
  return (
    <>
      <div className="header">
        <div className=" img-wrapper">
          <Header />
        </div>
      </div>
    </>
  );
};

export default Home;
