import SignUp from '../components/container/signUp/SignUp';

const signUp = () => {
  return (
    <div className="flex items-center justify-center h-screen img-wrapper">
      <SignUp />
    </div>
  );
};

export default signUp;
