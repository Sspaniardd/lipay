import Login from '../components/container/login/Login';

const login = () => {
  return (
    <div className="flex items-center justify-center h-screen img-wrapper">
      <Login />
    </div>
  );
};

export default login;
