module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    // Ensure these match with .storybook/preview.js
    screens: {
      xs: '375px',
      sm: '600px',
      md: '900px',
      lg: '1200px',
      xl: '1536px',
    },
    fontFamily: {
      sans: ['Arial', 'sans-serif'],
      serif: ['Garamond', 'serif'],
    },
    extend: {
      colors: {
        primary: {
          500: '#8C52FF',
          700: '#7538ed',
        },
        secondary: {
          main: '#C9E265',
        },
      },
      spacing: {
        128: '32rem',
      },
    },
  },
  plugins: [],
};
