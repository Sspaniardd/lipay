import { getApp, getApps, initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyBYZc6Oayg1rzNHnMY5ydZlv-tPCaWk-vY',
  authDomain: 'lipay-mati.firebaseapp.com',
  projectId: 'lipay-mati',
  storageBucket: 'lipay-mati.appspot.com',
  messagingSenderId: '281659881392',
  appId: '1:281659881392:web:a7ca28f98ed864e71e8875',
};

//Initializing firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
const db = getFirestore();
const auth = getAuth();

export default app;
export { auth, db };
