import { signInWithEmailAndPassword } from 'firebase/auth';
import { SubmitHandler, useForm } from 'react-hook-form';
import { auth } from '../../../firebase/clientApp';
import useAuth from '../../../hooks/useAuth';

export interface ILogin {
  email: string;
  phoneNumber?: number;
  password: string;
}

export type ILoginData = ILogin & React.ComponentPropsWithoutRef<'div'>;

const Login: React.FC<React.ComponentPropsWithoutRef<'div'>> = ({
  className,
  ...divProps
}) => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors, isSubmitting },
    reset,
  } = useForm<ILogin>();
  const { signIn } = useAuth();

  const onSubmit: SubmitHandler<ILogin> = async (data) => {
    const { email, password } = data;
    try {
      const res = await signInWithEmailAndPassword(auth, email, password);
      console.log(res.user);
    } catch (e) {
      alert('email already in use');
      console.log(e);
    }
  };

  return (
    <div
      {...divProps}
      className={`relative w-full h-full max-w-md p-4 md:h-auto ${className}`}
    >
      <div className="relative border rounded-lg shadow bg-none dark:bg-transparent border-primary-500">
        <div className="px-6 py-6 lg:px-8">
          <h3 className="mb-4 text-xl font-medium text-primary-500">Login</h3>
          <form
            className="space-y-6"
            action="#"
            onSubmit={handleSubmit(onSubmit)}
          >
            <div>
              <div className="flex justify-between">
                <label
                  typeof="email"
                  className="block mb-2 text-sm font-medium text-primary-500"
                >
                  Your email
                </label>
                <label
                  typeof="email"
                  className="block mb-2 text-sm font-medium text-primary-500"
                >
                  Use phone number?
                </label>
              </div>
              <input
                type="email"
                id="email"
                {...register('email', {
                  required: 'Email is required',
                  minLength: { value: 4, message: 'Invalid Email' },
                })}
                className="bg-secondary-main border border-primary-500 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 "
                placeholder="name@company.com"
                required
              />
            </div>
            <div>
              <label
                typeof="password"
                className="block mb-2 text-sm font-medium text-primary-500"
              >
                Your password
              </label>
              <input
                type="password"
                id="password"
                placeholder="••••••••"
                {...register('password', {
                  required: 'Password is required',
                  minLength: {
                    value: 6,
                    message: 'Password should have minimum 6 characters',
                  },
                })}
                className="bg-secondary-main border border-primary-500 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 "
                required
              />
            </div>

            <button
              type="submit"
              className="w-full text-white bg-primary-500 hover:bg-primary-700  focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
            >
              Login
            </button>
            <div className="text-sm font-medium text-gray-500">
              Not registered?{' '}
              <button
                type="button"
                className="size-small h-8 text-white bg-primary-500 hover:bg-primary-700  focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-1.5 text-center"
              >
                Create Account
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
