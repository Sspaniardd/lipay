import { IBasicModal } from './BasicModal';

const base: IBasicModal = {
  handleShowModal: () => {},
  showModal: true,
};

export const mockBasicModalProps = {
  base,
};
