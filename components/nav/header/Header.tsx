import Link from 'next/link';

export interface IHeader extends React.ComponentPropsWithoutRef<'header'> {}

const Header: React.FC<IHeader> = ({ className, ...headerProps }) => {
  return (
    <header
      {...headerProps}
      className={`w-full flex flex-row justify-between ${className}`}
    >
      <div className="m-5 space-x-5">
        <a>Burger</a>
      </div>
      <div className="m-5 space-x-5">
        <Link href="/">
          <a className=" hover:underline">Login</a>
        </Link>
        <Link href="/">
          <a className=" hover:underline">SignUp</a>
        </Link>
      </div>
    </header>
  );
};

export default Header;
